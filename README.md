# cloning

Practice of tailwind with eleventy

## Getting started

- Clone the repo

- In one tab of the terminal run : 
    - ``` npm i ```

- In another new tab of the terminal, run :
    - ``` npx tailwindcss -i ./tailwind.css -o ./_site/css/styles.css --watch ```
    
- Finally in the first tab of the terminal, run :
    - ``` npm start ```

- Now open this link [http://localhost:8080](http://localhost:8080) in the browser to see the result
